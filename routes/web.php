<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Models\MainPage;
use App\Models\Publications;
use App\Models\Menus;
use App\Http\Controllers\ChangeLangController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
Route::redirect('/','/kz/main');

Route::get('/video',function(){
    $main_widgets = MainPage::all()->first()->video;
    return view('video',["video"=>$main_widgets]);
});


//Route::get('/news/{id}', [ChangeLangController::class,'news_detail'])->name('news_detail');
Route::group(['prefix'=>'{lang}',], function () {

    Route::get('/news/{id}', [ChangeLangController::class,'news_detail'])->name('news_detail');
    //Route::get('/test', [ChangeLangController::class,'news_detail'])->name('news_detail');

//    Route::get('news/detail/{id}', [ChangeLangController::class,'news_detail'])->name('news_detail');
    Route::get('/main',[ChangeLangController::class,'main'])->name('main');
//Route::get('/main', function () {
//        if (app()->getLocale() == 'kz') {
////   \Illuminate\Support\Facades\App::setLocale('ru');
//            $main_widgets = MainPage::all()->translate('kz', 'ru')->first();
////    $menus = App\Models\Menus::all()->translate('kz', 'ru');;
////    $menus = Menus::select('menu_title')->get()->first();
////    $menus = Menus::all()->translate('kz','ru')->get();
//            // $menus = Menus::select('menu_title')->first();
////    dd($menus);
//            $social_logos = App\Models\SocialLogos::all();
//            $carousel_images = App\Models\CarouselImages::all();
//
//            $perspectives = App\Models\Perspectives::all()->translate('kz', 'ru')->first();
//            $posters = App\Models\Posters::all()->translate('kz', 'kz');;
//            $footer = App\Models\FooterPage::all()->first()->translate('kz', 'ru');
//            $logos = App\Models\Logos::all();
//            $perspective_items = App\Models\PerspectiveItems::all()->translate('kz', 'ru');;
//
//            }else{
//            $main_widgets = MainPage::all()->translate('ru', 'kz')->first();
//
//            $social_logos = App\Models\SocialLogos::all();
//            $carousel_images = App\Models\CarouselImages::all();
//
//            $perspectives = App\Models\Perspectives::all()->translate('ru', 'kz')->first();
//            $posters = App\Models\Posters::all()->translate('ru', 'kz');;
//            $footer = App\Models\FooterPage::all()->first()->translate('ru', 'kz');
//            $logos = App\Models\Logos::all();
//            $perspective_items = App\Models\PerspectiveItems::all()->translate('ru', 'kz');;
//        }
    //dd($menus);
//    $kz = \App\Models\Posters::withTranslation('kz')->get();
//    if (Voyager::translatable($products)) {
//        $products = $products->translate($locale, 'en');
//    }
//    return view('main',[
//        "index"=>0,
//        "main_widgets"=>$main_widgets,
//        "social_logos"=>$social_logos,
//        "carousel_images"=>$carousel_images,
//        "perspectives"=>$perspectives,
//        "posters"=>$posters,
//        "logos"=>$logos,
////        "menus"=>$menus,
//        "footer"=>$footer,
//        "perspective_items"=>$perspective_items,
//
//    ]);
//})->name('main');
Route::get('/about',[ChangeLangController::class,'about'])->name('about');
//Route::get('/about',function(){
////    if (app()->getLocale() == 'kz') {
//////    \Illuminate\Support\Facades\App::setLocale('kz');
////        $main_widgets = MainPage::all()->translate('kz', 'ru')->first();
////        $footer = App\Models\FooterPage::all()->translate('kz', 'ru')->first();
////        $content = App\Models\AboutPage::all()->translate('kz', 'ru')->first();
////    } else{
////        $main_widgets = MainPage::all()->translate('ru', 'kz')->first();
////        $footer = App\Models\FooterPage::all()->translate('ru', 'kz')->first();
////        $content = App\Models\AboutPage::all()->translate('ru', 'kz')->first();
////    }
////    return view('about',[
////        "index"=>1,
////        "main_widgets"=>$main_widgets,
////        "content"=>$content,
////        "footer"=>$footer]);
////})->name('about');
//Route::get('/contacts',function() {
//    if (app()->getLocale() == 'kz'){
////    \Illuminate\Support\Facades\App::setLocale('kz');
//        $main_widgets = MainPage::all()->translate('kz', 'ru')->first();
//        $footer = App\Models\FooterPage::all()->translate('kz', 'ru')->first();
//}
//    else{
//        $main_widgets = MainPage::all()->translate('ru', 'kz')->first();
//        $footer = App\Models\FooterPage::all()->translate('ru', 'kz')->first();
//    }
//    return view('contacts',[
//        "index"=>5,
//        "main_widgets"=>$main_widgets,
//        "footer"=>$footer]);
//})->name('contacts');

    Route::get('/contacts',[ChangeLangController::class,'contacts'])->name('contacts');

//Route::get('/services',function(){
//    if (app()->getLocale() == 'kz') {
////    \Illuminate\Support\Facades\App::setLocale('kz');
//        $main_widgets = MainPage::all()->translate('kz', 'ru')->first();
//        $footer = App\Models\FooterPage::all()->translate('kz', 'ru')->first();
//        $content = App\Models\ServicePage::all()->translate('kz', 'ru')->first();
//    }
//    else{
//        $main_widgets = MainPage::all()->translate('ru', 'kz')->first();
//        $footer = App\Models\FooterPage::all()->translate('ru', 'kz')->first();
//        $content = App\Models\ServicePage::all()->translate('ru', 'ru')->first();
//    }
//    return view('services',[
//        "index"=>3,
//        "main_widgets"=>$main_widgets,
//        "content"=>$content,
//        "footer"=>$footer]);
//})->name('services');
Route::get('/services',[ChangeLangController::class,'services'])->name('services');
//Route::get('/park',function(){
//    if (app()->getLocale() == 'kz'){
//        $main_widgets = MainPage::all()->first();
//        $footer = App\Models\FooterPage::all()->first()->translate('kz','ru');
//        $content = App\Models\ParkPage::all()->translate('kz','ru')->first();
//    }else{
//        $main_widgets = MainPage::all()->first();
//        $footer = App\Models\FooterPage::all()->first()->translate('ru','ru');
//        $content = App\Models\ParkPage::all()->translate('ru','ru')->first();
//    }
//    \Illuminate\Support\Facades\App::setLocale('kz');

//    return view('park',[
//        "index"=>2,
//        "main_widgets"=>$main_widgets,
//        "content"=>$content,
//        "footer"=>$footer]);
//})->name('park');
Route::get('/park',[ChangeLangController::class,'park'])->name('park');
Route::get('/news',[ChangeLangController::class,'news'])->name('news');
//Route::get('/news/{id}', [ChangeLangController::class,'news_detail'])->name('news_detail');
//Route::get('news/detail/{id}', [ChangeLangController::class,'news_detail'])->name('news_detail');


//Route::get('/news/detail/{id}',function($id){
//    $publication = Publications::find($id);
//    $search_code = $publication->post_code;
//    $same_posts = Publications::all()
//            ->where('post_code',$search_code)
//            ->take(3);
//    $main_widgets = MainPage::all()->first();
//    $footer = App\Models\FooterPage::all()->first();
//    return view('news_inner',[
//        "index"=>4,
//        "publication"=>$publication,
//        "same_posts"=>$same_posts,
//        "main_widgets"=>$main_widgets,
//        "footer"=>$footer]);
//});

//Route::get('/rent',function(){
//    if (app()->getLocale() == 'kz'){
//        $index = 0;
//        $footer = App\Models\FooterPage::all()->translate('kz','ru')->first();
//        $main_widgets = MainPage::all()->translate('kz','ru')->first();
//        $rents = App\Models\RentPublication::all()->translate('kz','kz')->first();;
//    }else{
//        $index = 0;
//        $footer = App\Models\FooterPage::all()->translate('ru','ru')->first();
//        $main_widgets = MainPage::all()->translate('ru','ru')->first();
//        $rents = App\Models\RentPublication::all()->translate('ru','kz')->first();;
//    }
//    \Illuminate\Support\Facades\App::setLocale('kz');

//    return view('rend',compact('index','footer','main_widgets','rents'));
//})->name('/rent');
    Route::get('/rent',[ChangeLangController::class,'rent'])->name('rent');
});



Route::view('/welcome',function (){
    \Illuminate\Support\Facades\App::setLocale('kz');
    $welcome = Menus::all();
    return view('welcome',['welcome'=>$welcome]);
});
