const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [])
    .sass('resources/sass/main.sass', 'public/css')
    .sass('resources/sass/block.sass', 'public/css')
    .sass('resources/sass/about.sass', 'public/css')
    .sass('resources/sass/page/news_inner.scss','public/css')
    .sass('resources/sass/page/news.scss','public/css')
    .sass('resources/sass/page/park.scss','public/css')
    .sass('resources/sass/page/services.scss','public/css')
    .sass('resources/sass/plugins/media.scss','public/css')
    .sass('resources/sass/plugins/text.scss','public/css')
    .sass('resources/sass/main2.scss','public/css')
    .sass('resources/sass/typography/fonts.scss','public/css');
