<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerspectivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perspectives', function (Blueprint $table) {
            $table->id();
            $table->text('title');
            $table->text('description_text1');
            $table->text('description_title1');
            $table->text('description_img1');
            $table->text('description_text2');
            $table->text('description_title2');
            $table->text('description_img2');
            $table->text('image');
            $table->text('right_title');
            $table->text('right_content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perspectives');
    }
}
