<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParkPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('park_pages', function (Blueprint $table) {
            $table->id();
            $table->text('content1');
            $table->text('image_first');
            $table->text('image_second');
            $table->text('image_third');
            $table->text('content2');
            $table->text('image_fourth');
            $table->text('content3');
            $table->text('image_fifth');
            $table->text('image_sixth');
            $table->text('content4');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('park_pages');
    }
}
