<?php

namespace App\Http\Controllers;

use App\Models\AboutPage;
use App\Models\Banners;
use App\Models\MainPage;
use App\Models\ParkPage;
use App\Models\Publications;
use App\Models\RentPublication;
use App\Models\ServicePage;
use Illuminate\Http\Request;
use App\Models\SocialLogos;
use App\Models\CarouselImages;
use App\Models\Perspectives;
use App\Models\FooterPage;
use App\Models\PerspectiveItems;
use App\Models\Logos;
use App\Models\Posters;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class   ChangeLangController extends Controller
{
    //
    public function main()
    {

        if (app()->getLocale() == 'kz') {
//   \Illuminate\Support\Facades\App::setLocale('ru');
            $main_widgets = MainPage::all()->translate('kz', 'ru')->first();
//    $menus = App\Models\Menus::all()->translate('kz', 'ru');;
//    $menus = Menus::select('menu_title')->get()->first();
//    $menus = Menus::all()->translate('kz','ru')->get();
            // $menus = Menus::select('menu_title')->first();
//    dd($menus);
            $social_logos = SocialLogos::all();
            $carousel_images = CarouselImages::all();

            $perspectives = Perspectives::all()->translate('kz', 'ru')->first();
            $posters = Posters::all()->translate('kz', 'kz');;
            $footer = FooterPage::all()->first()->translate('kz', 'ru');
            $logos = Logos::all();
            $perspective_items = PerspectiveItems::all()->translate('kz', 'ru');;

        } else {
            $main_widgets = MainPage::all()->translate('ru', 'kz')->first();

            $social_logos = SocialLogos::all();
            $carousel_images = CarouselImages::all();

            $perspectives = Perspectives::all()->translate('ru', 'kz')->first();
            $posters = Posters::all()->translate('ru', 'kz');;
            $footer = FooterPage::all()->first()->translate('ru', 'kz');
            $logos = Logos::all();
            $perspective_items = PerspectiveItems::all()->translate('ru', 'kz');;
        }
        return view('main', [
            "index" => 0,
            "main_widgets" => $main_widgets,
            "social_logos" => $social_logos,
            "carousel_images" => $carousel_images,
            "perspectives" => $perspectives,
            "posters" => $posters,
            "logos" => $logos,
//       "menus"=>$menus,
            "footer" => $footer,
            "perspective_items" => $perspective_items,

        ]);
    }

    public function about()
    {
        if (app()->getLocale() == 'kz') {
//    \Illuminate\Support\Facades\App::setLocale('kz');
            $banners = Banners::all()->first();
            $main_widgets = MainPage::all()->translate('kz', 'ru')->first();
            $footer = FooterPage::all()->translate('kz', 'ru')->first();
            $content = AboutPage::all()->translate('kz', 'ru')->first();
        } else {
            $banners = Banners::all()->first();
            $main_widgets = MainPage::all()->translate('ru', 'kz')->first();
            $footer = FooterPage::all()->translate('ru', 'kz')->first();
            $content = AboutPage::all()->translate('ru', 'kz')->first();
        }
        return view('about', [
            "index" => 1,
            "banners" => $banners,
            "main_widgets" => $main_widgets,
            "content" => $content,
            "footer" => $footer,
        ]);
    }

    public function contacts()
    {
        $images = ParkPage::all()->first();
        if (app()->getLocale() == 'kz') {
//    \Illuminate\Support\Facades\App::setLocale('kz');
            $main_widgets = MainPage::all()->translate('kz', 'ru')->first();
            $footer = FooterPage::all()->translate('kz', 'ru')->first();
        } else {
            $main_widgets = MainPage::all()->translate('ru', 'kz')->first();
            $footer = FooterPage::all()->translate('ru', 'kz')->first();
        }
        return view('contacts',[
            "images" => $images,
            "index"=>5,
           "main_widgets"=>$main_widgets,
           "footer"=>$footer]);
    }

    public function services(){
        $images = ParkPage::all()->first();
        if (app()->getLocale() == 'kz') {
//    \Illuminate\Support\Facades\App::setLocale('kz');

            $main_widgets = MainPage::all()->translate('kz', 'ru')->first();
            $footer = FooterPage::all()->translate('kz', 'ru')->first();
            $content = ServicePage::all()->translate('kz', 'ru');
        }
        else{
            $main_widgets = MainPage::all()->translate('ru', 'kz')->first();
            $footer = FooterPage::all()->translate('ru', 'kz')->first();
            $content = ServicePage::all();
        }
        return view('services',[
            'images' => $images,
            "index"=>3,
        "main_widgets"=>$main_widgets,
        "content"=>$content,
        "footer"=>$footer
        ]);

    }

    public function park(){

        if (app()->getLocale() == 'kz'){
            $publications = Publications::all()->first();
            $main_widgets = MainPage::all()->first();
            $footer = FooterPage::all()->first()->translate('kz','ru');
            $content = ParkPage::all()->translate('kz','ru')->first();
        }else{
            $publications = Publications::all()->first();
            $main_widgets = MainPage::all()->first();
            $footer = FooterPage::all()->first()->translate('ru','ru');
            $content = ParkPage::all()->translate('ru','ru')->first();
        }
        return view('park',[
            "index"=>2,
            "main_widgets"=>$main_widgets,
            "content"=>$content,
            "footer"=>$footer,
            "publications" => $publications
    ]);
    }

    public function rent(){

        if (app()->getLocale() == 'kz'){
            $index = 0;
            $footer = FooterPage::all()->translate('kz','ru')->first();
            $main_widgets = MainPage::all()->translate('kz','ru')->first();
            $rents = RentPublication::all()->translate('kz','kz');
        }else{
            $index = 0;
            $footer = FooterPage::all()->translate('ru','ru')->first();
            $main_widgets = MainPage::all()->translate('ru','ru')->first();
            $rents = RentPublication::all()->translate('ru','kz');;
        }
        return view('rend',compact('index','footer','main_widgets','rents'));
    }

    public function news(){
        $images = ParkPage::all()->first();
        if (app()->getLocale() == 'kz') {
            $news = Publications::paginate(3)->translate('kz');
            $main_widgets = MainPage::all()->translate('kz', 'ru')->first();
            $footer = FooterPage::all()->translate('kz', 'ru')->first();
        }else{
            $news = Publications::simplePaginate(3)->translate('ru');
            $main_widgets = MainPage::all()->translate('ru', 'kz')->first();
            $footer = FooterPage::all()->translate('ru', 'kz')->first();
        }

        return view('news',
            [
                "images"=>$images,
                "men"=>$news,
                "index"=>4,
                "main_widgets"=>$main_widgets,
                "footer"=>$footer
            ]);
    }
    public function news_detail(Request  $request){

//        $post = Publications::where('id',)->first();
        //$post = DB::table('publications')->where('id',1);
        //dd($post);


        if (app()->getLocale() == 'kz') {
            $posts = Publications::where('id',$request->id)->first()->translate('kz','ru');
//            $posts = Publications::all();
//            dd($posts);
//        dd($publication->post_title);
            // $search_code = $publication->post_code;
           //$same_posts = Publications::find()->translate('kz','ru');
//        $same_posts = Publications::all()
//            ->where('post_code',$search_code)
//            ->take(3);
            //$posts = Publications::all()->translate('kz','ru');
//            $posts = Publications::find(1)->translate('kz');
//            $same_posts = Publications::inRandomOrder()->paginate(3)->translate('kz','ru');
//            $same_posts = Publications::whereNotIn('id',[$request->id])->translate('kz','ru')->inRandomOrder()->limit(3);
            $same_posts = Publications::inRandomOrder()->paginate(3)->translate('kz','ru');

            $main_widgets = MainPage::all()->translate('kz','ru')->first();
            $footer = FooterPage::all()->translate('kz','ru')->first();
        }else{
            $posts = Publications::where('id',$request->id)->first()->translate('ru');
            $same_posts = Publications::inRandomOrder()->paginate(3)->translate('ru','ru');
            $main_widgets = MainPage::all()->translate('ru','ru')->first();
            $footer = FooterPage::all()->translate('ru','ru')->first();
        }


        return view('news_inner',[
            "index"=>4,
            "posts"=>$posts,
            "same_posts"=>$same_posts,
            "main_widgets"=>$main_widgets,
            "footer"=>$footer
        ]);
    }

}


