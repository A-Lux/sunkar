<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VoyagerController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function index(Request $request)
    {
        return $this->show($request, 1);
    }
}
