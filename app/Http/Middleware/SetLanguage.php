<?php

namespace App\Http\Middleware;

use App\Models\Perspectives;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class SetLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        App::setLocale($request->lang);
//        if($request->lang == 'kz'){
//        if (App::currentLocale() == 'kz'){
////             MainPage::all()->translate('kz','ru')->first();
//             Perspectives::all()->translate('kz','ru')->first();
//        }
        return $next($request);
    }
}
