<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Publications extends Model
{
    use HasFactory, Translatable;
    protected $translatable = ['post_title','post_text'];
}
