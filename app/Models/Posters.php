<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Posters extends Model
{
    use Translatable, HasFactory;
    protected $translatable = ['title','distance','places','width'];
}
