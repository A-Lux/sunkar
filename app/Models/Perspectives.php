<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Perspectives extends Model
{
    use Translatable, HasFactory;
    protected $translatable = ['title','description_text1','description_title1','description_text2','description_title2','right_title','right_content'];
}
