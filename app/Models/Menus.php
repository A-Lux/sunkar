<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Menus extends Model
{
    use Translatable, HasFactory;
    protected $translatable = ['menu_title'];
}
