<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class ParkPage extends Model
{
    use Translatable, HasFactory;
    protected $translatable = ['content1', 'content2','content3','content4'];
}
