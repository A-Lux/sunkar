@extends('welcome')
@section('style')
    <link rel="stylesheet" href="{{ asset('css/rent.css') }}">
@endsection
@section('content')
<div class="container">
    <div class="rent-title">{{__('welcome.button')}}</div>
    <div class="block-rent">
        @foreach($rents as $rent)
            <div class="item-rent-main">
                <div class="item-rent">
                    <div class="item-img">
                        <img src="{{ url('storage').'/'.$rent->image }}" alt="">
                    </div>
                    <div class="item-title-rent change_">{!! $rent->title !!}</div>
                    <div class="item-text-name-rent change_">{!! $rent->description !!}</div>
                    <div class="item-btn-name-rent">
                        <a href="tel:87774445632">{{__('welcome.call')}}</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
