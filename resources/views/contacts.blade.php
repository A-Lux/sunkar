@extends('welcome')
@section('style')
    <link rel="stylesheet" href="{{ asset('css/about.css') }}">
@endsection
@section('content')
<div class="news">
    <div class="news_bg" style="background-image: url({{ \TCG\Voyager\Facades\Voyager::image($images->image_fourth)}}"></div>
    <div class="news_content">
      <div class="container">
        <div class="row">
            <div class="col-md-12" style="padding: 0;">
                <div class="global_title">
                    <h3>{{__('welcome.contacts')}}</h3>
                </div>
                <div class="content">
                    <div class="content_inner">
                        <!-- Changed font Size for Contacts Page -->
                        <h4 class="change_">{{$footer->address}}</h4>
                        <h5 class="change_">{{$footer->phone}}</h5>
{{--                        <p class="change_">{{ $footer->address }}</p>--}}
{{--                        <p class="change_">{{ $footer->phone }}</p>--}}
                        <div class="map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2908.7041072196716!2d76.88752341547749!3d43.19471907913977!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3883689bd62441a5%3A0x1d58a0fbe8591a5a!2sAl-Farabi%20Avenue%20128%2C%20Almaty!5e0!3m2!1sen!2skz!4v1606217802351!5m2!1sen!2skz" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
