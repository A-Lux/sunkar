@extends('welcome')
@section('style')
    <link rel="stylesheet" href="{{ asset('css/services.css') }}">
@endsection

@section('content')

<div class="news">
    <div class="news_bg" style="background-image: url({{ \TCG\Voyager\Facades\Voyager::image($images->image_first)}}"></div>
    <div class="news_content">
      <div class="container">
        <div class="row">
          <div class="col-md-12" style="padding: 0;">
            <div class="global_title">
              <h3>{{__('welcome.services')}}</h3>
            </div>
            <div class="services_content">

                @foreach($content as $service)
                    <br>
                <h2 class="text-center">{{$service->contentTitle}}</h2>
                    <br>
{{--                {{$service->contentText}}--}}
                <h5>{{$service->contentText}}</h5>
                    <br>
                <img src="{{\TCG\Voyager\Facades\Voyager::image($service->backgroundImage1)}}" alt="">
                    <br>
{{--               {{\TCG\Voyager\Voyager::image($content->backgroundImage1)}}--}}
{{--                {{$content->contentText}}--}}
{{--              <p>--}}
{{--                Самые крупные масштабные соревнование это Зимняя Азиатав 2010году и  28 Зимняя Всемирная Универсиада. Сункар принял 8 стран, которые соревновались в лыжном- двоеборий и 16 стран прыжки на лыжах с трамплина, всего 811 спортсменов.--}}
{{--                Рекорд трамплина был установлен во время континентального Кубка 25.10.2010г. польским прыгуном  Камилем Штоком (трамплин К-95 – 104 метра, К-125 – 137 метров). Во время Чемпионата РК 09.10.2011 года казахстанский прыгун участник  Олимпийских Игр  и чемпионатов мира  Николай Корпенко на трамплине К-125 установил рекорд прыгнув на 139 метров . В 2013--}}
{{--              </p>--}}
{{--              <p>Самые крупные масштабные соревнование это Зимняя Азиатав 2010году и  28 Зимняя Всемирная Универсиада. Сункар принял 8 стран, которые соревновались в лыжном- двоеборий и 16 стран прыжки на лыжах с трамплина, всего 811 спортсменов. </p>--}}

{{--              @foreach($content as $val)--}}
{{--                @if($loop->index % 4 == 0)--}}
{{--                  <div class="services_content_mini" style="background-image: url('{{ url('storage').'/'.$val->backgroundImage1 }}');">--}}
{{--                    <div class="row" style="margin: 0px;">--}}
{{--                      <div class="col-md-6 white_stealth" style="padding: 0;">--}}
{{--                        <h3 class="change_">{!! $val->contentTitle !!}</h3>--}}
{{--                        <p class="change_">{!! $val->contentText !!}</p>--}}
{{--                      </div>--}}
{{--                      <div class="col-md-6" style="padding: 0;"></div>--}}
{{--                    </div>--}}
{{--                  </div>--}}
{{--                @else--}}
{{--                  <div class="services_content_mini" style="background-image: url('{{ url('storage').'/'.$val->backgroundImage1 }}');">--}}
{{--                    <div class="row" style="margin: 0px;">--}}
{{--                      <div class="col-md-6" style="padding: 0;"></div>--}}
{{--                      <div class="col-md-6 white_stealth" style="padding: 0;">--}}
{{--                        <h3 class="change_">{!! $val->contentTitle !!}</h3>--}}
{{--                        <p class="change_">{!! $val->contentText !!}</p>--}}
{{--                      </div>--}}
{{--                    </div>--}}
{{--                  </div>--}}
{{--                @endif--}}
{{--              @endforeach--}}
                @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
