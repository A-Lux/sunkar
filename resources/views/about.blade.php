@extends('welcome')
@section('style')
    <link rel="stylesheet" href="{{ asset('css/about.css') }}">
@endsection
@section('content')
<div class="news">
{{--    <div class="news_bg" style="background-image: url('{{ asset('storage/'.\App\Models\Banners::all()->first()["about-page"]) }}');"></div>--}}
{{--    <div class="news_bg" style="background-image: url('{{ asset('storage/'.\App\Models\Banners::all()->first()["about-page"]) }}');"></div>--}}
{{--    <div class="news_bg" style="background-image: url('{{ asset('storage/'.\App\Models\Banners::all()->first()["about-page"]) }}');"></div>--}}
{{--    <div class="news_bg" style="background-image: url('{{ asset('storage/'. $banners['about-page'] )}}')"></div>--}}
        <div class="news_bg" style="background-image: url({{ \TCG\Voyager\Facades\Voyager::image($content->description_image)}}"></div>
{{--    <div class="news_bg" style="background-image: url("{{\TCG\Voyager\Facades\Voyager::image($banners->about-page)}}")"></div>--}}
{{--<div class="backgroundImage" style="background-image: url('{{ \TCG\Voyager\Facades\Voyager::image($val->image) }}')"></div>--}}
    <div class="news_content">
      <div class="container">
        <div class="row">
            <div class="col-md-12" style="padding: 0;">
                <div class="global_title">
                    <h3>{{__('welcome.about-us')}}</h3>
                </div>
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="content_inner">
                                <p class="change_">{!! $content->description !!}</p>
                                <div class="map" style="height: 60vh;background-image: url('{{ \TCG\Voyager\Facades\Voyager::image($content->description_image) }}')">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
