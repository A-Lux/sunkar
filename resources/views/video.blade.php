<div>
    <style>
        #video{
            width: 100%;
            height: 100%;
            position: fixed;
            margin: 0 auto;
            left: 0;
            top: 0;
        }
    </style>
    @php
        $m = str_replace("watch?v=","embed/",$video);

    @endphp
    <iframe style="width: 100%;height: 100%;position: fixed;
    left: 0;top: 0;"
    src="{{ $m }}"
    frameBorder="0">
    </iframe>
    {{-- <video src="{{ url('storage'.'/'.$m->download_link) }}" 
        controls
        autoplay
        id="video">
    </video> --}}
</div>