@extends('welcome')
@section('style')
    <link rel="stylesheet" href="{{ asset('css/news_inner.css') }}">
@endsection
@section('content')
<div class="news_inner">
    <div class="news_inner_bg" style="background-image: url('{{ asset('img/img/news_bg.png') }}')"></div>
    <div class="news_inner_content">
      <div class="container">
        <div class="row">
          <div class="col-md-12" style="padding: 0;">
            <div class="news_inner_content_title">
{{--              <h3 class="change_">{{ $publication->post_title }}</h3>--}}
              <h3 class="change_">{{ $posts->post_title }}</h3>
              <p class="change_">
              {{ Carbon\Carbon::parse($posts->created_at)->day }}/
              {{ Carbon\Carbon::parse($posts->created_at)->month }}/
              {{ Carbon\Carbon::parse($posts->created_at)->year }}
              </p>
            </div>
            <div class="news_inner_content_descr change_">
              {{ $posts->post_text }}
              <img src="{{ asset('storage/'.$posts->post_image) }}" alt="">
            </div>
            <div class="news_inner_content_bottom">
              <h3 class="change_">{{__('welcome.similar')}}</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="news_inner_footer">
      <div class="container-fluid">
        <div class="row">
          @foreach($same_posts as $vals)
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <a href="{{ url('news/detail'.'/'.$vals->id) }}">
            <a href="{{ url('/news'.'/'.$vals->id) }}">
            <a href="{{ url('/news'.'/'.$vals->id) }}">
{{--                <a href="{{url( 'app()->getLocale()' . '/' .'news/' . $vals->id}}">--}}
                <a href="{{route('news_detail',['lang'=>app()->getLocale(), 'id'=>$vals->id])}}">
              <div class="news_inner_footer_items">
                <div class="news_inner_footer_items_img">
                  <img src="{{ asset('storage/'.$vals->post_image) }}" alt="">
                </div>
                <div class="news_inner_footer_items_descr">
                  <h3 class="change_">
                    {{ $vals->post_title }}
                  </h3>
                  <p id="post_text change_">
                     {{ $vals->post_text }}
                  </p>
                </div>
                <div class="news_inner_footer_items_date">
                  <span class="change_">
                  {{ Carbon\Carbon::parse($vals->created_at)->day }}/
                  {{ Carbon\Carbon::parse($vals->created_at)->month }}/
                  {{ Carbon\Carbon::parse($vals->created_at)->year }}
                  </span>
                </div>
              </div>
              </a>
            </div>
          @endforeach

        </div>
      </div>

    </div>
  </div>
@endsection
