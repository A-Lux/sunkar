<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('/css/main.css').(env('APP_DEBUG') === true ? '?'.uniqid() : '123') }}">
        <link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/zirafa/bootstrap-grid-only/94433673/css/grid12.css">
        <link href="https://fonts.googleapis.com/css2?family=Ropa+Sans&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('/css/main2.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        @yield('style')
        <title>SUNKAR</title>
    </head>
    <body>

{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-2 col-md-offset-6 text-right">--}}
{{--                <strong>Choose Your Lang: </strong>--}}
{{--            </div>--}}
{{--            <div class="col-md-4">--}}
{{--                <select name="" class="form-control changeLang" id="">--}}
{{--                    <option value="ru"{{session()->get('locale') == 'ru' ? 'selected': ''}}>Russian</option>--}}
{{--                    <option value="kz"{{session()->get('locale') == 'kz' ? 'selected': ''}}>Kazakh</option>--}}
{{--                </select>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <script type="text/javascript">--}}

{{--        var url = "{{ route('changeLang') }}";--}}

{{--        $(".changeLang").change(function(){--}}
{{--            window.location.href = url + "?lang="+ $(this).val();--}}
{{--        });--}}

{{--    </script>--}}
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="header_inner">
                        <div class="hedaer_inner_logo">
                            <a href="{{ route('main', app()->getLocale())}}">
{{--                                <img src="{{ url('storage/'.$main_widgets->logo) }}" alt="">--}}
                                <img src="{{\TCG\Voyager\Facades\Voyager::image($main_widgets->logo)}}" alt="">
                            </a>
                        </div>
                        <div class="header_inner_menu">

                            <input type="hidden" id="hiddenIndex" value="{{ $index }}">
                            <ul>
                                <li>
{{--                                    <a href="{{ app()->getLocale(), url('/main') }}">{{__('welcome.main')}}</a>--}}
{{--                                    <a href="{{ url('/services', app()->getLocale()) }}">{{__('welcome.services')}}</a>--}}
                                    <a href="{{route('main', app()->getLocale())}}">{{__('welcome.main')}}</a>

                                </li>
                                <li>
{{--                                    <a href="{{ url('/about', app()->getLocale()) }}">{{__('welcome.project')}}</a>--}}
{{--                                    <a href="{{ url('/about', app()->getLocale()) }}">{{__('welcome.project')}}</a>--}}
{{--                                    <a href="{{  app()->getLocale()->url('/test') }}">{{__('welcome.project')}}</a>--}}
                                    <a href="{{route('about', app()->getLocale())}}">{{__('welcome.project')}}</a>

                                </li>
                                <li>
{{--                                    <a href="{{ url('/park', app()->getLocale()) }}">{{__('welcome.park')}}</a>--}}
                                    <a href="{{route('park', app()->getLocale())}}">{{__('welcome.park')}}</a>
                                </li>
                                <li>
                                    <a href="{{route('services', app()->getLocale())}}">{{__('welcome.services')}}</a>
                                </li>
                                <li>
                                    <a href="{{route('news', app()->getLocale())}}">{{__('welcome.news')}}</a>
                                </li>
                                <li>
                                    <a href="{{route('contacts', app()->getLocale())}}">{{__('welcome.contacts')}}</a>
                                </li>
{{--                                <li>--}}
{{--                                    <a href="{{route(\Illuminate\Support\Facades\Route::currentRouteName(),'')}}"></a>--}}
{{--                                </li>--}}
                                <li class="nav-item nav-item_language">
                                    <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(),['lang' => 'kz', 'id' => request('id')])  }}" class="nav-link">KZ</a>
                                </li>
                                <li class="nav-item nav-item_language">
                                    <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), ['lang' => 'ru', 'id' => request('id')])  }}" class="nav-link">RU</a>
                                </li>
                            </ul>

                        </div>
                        <div class="burger">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                        <div class="adaptive_menu">
                            <i class="fa fa-close"></i>
                            <ul>
                                <li>
{{--                                    <a href="{{ url('/main'),app()->getLocale() }}" class="adaptive_li">{{__('welcome.main')}}</a>--}}
                                    <a href="{{route('main', app()->getLocale())}}"  class = "adaptive_li" >{{__('welcome.main')}}</a>
                                </li>
                                <li>
{{--                                    <a href="{{ url('/about'),app()->getLocale() }}" class="adaptive_li">{{__('welcome.project')}}</a>--}}
                                    <a href="{{route('about', app()->getLocale())}}" class = "adaptive_li">{{__('welcome.project')}}</a>
                                </li>
                                <li>
{{--                                    <a href="{{ url('/park'),app()->getLocale() }}" class="adaptive_li">{{__('welcome.park')}}</a>--}}
                                    <a href="{{route('park', app()->getLocale())}}" class = "adaptive_li">{{__('welcome.park')}}</a>
                                </li>
                                <li>
{{--                                    <a href="{{ url('/services'),app()->getLocale() }}" class="adaptive_li">{{__('welcome.services')}}</a>--}}
                                    <a href="{{route('services', app()->getLocale())}}" class = "adaptive_li">{{__('welcome.services')}}</a>
                                </li>
                                <li>
{{--                                    <a href="{{ url('/news'),app()->getLocale() }}" class="adaptive_li">{{__('welcome.news')}}</a>--}}
                                    <a href="{{route('news', app()->getLocale())}}" class = "adaptive_li">{{__('welcome.news')}}</a>
                                </li>
                                <li>
{{--                                    <a href="{{ url('/contacts'),app()->getLocale() }}" class="adaptive_li">{{__('welcome.contacts')}}</a>--}}
                                    <a href="{{route('contacts', app()->getLocale())}}" class = "adaptive_li">{{__('welcome.contacts')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), ['lang' => 'ru', 'id' => request('id')])  }}" class="nav-link">RU</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(),['lang' => 'kz', 'id' => request('id')])  }}" class="nav-link">KZ</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-body">
            <div class="modal-inner">
                <i class="fa fa-close close-modal-body"></i>

                <form action="">
                    <p>
                        <input type="text" name="name" id="name" placeholder="Имя">

                    </p>
                    <p>
                        <input type="email" name="email" id="email" placeholder="Почта">

                    </p>
                    <p>
                        <input type="text" name="phone" id="phone" placeholder="Номер">
                    </p>
                    <p>
                        <textarea name="text" id="textarea" cols="30" rows="10" placeholder="Текст"></textarea>
                    </p>
                    <p>
                        <input type="submit" id="submit" value="Отправить">
                    </p>
                </form>
            </div>
        </div>
        @yield('content')

        <div class="footer">
            <div class="footer_inner">
                <div class="container">
                    <div class="row">
                        <div class="footer_inner_left">
                            <div class="img"></div>
                            <div class="content _change">
                                {{ $footer->address }}
                            </div>
                        </div>
                        <div class="footer_inner_right ">
                            <p class="_change">{{ $footer->phone }}</p>
                            <button class="modal-appear" href="#">{{__('welcome.text-us')}}</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_outer">
                <div class="container">
                    <div class="row">
                        <div class="logo">
                            <img src="{{ url('storage/').'/'.$footer->footLogo }}" alt="">
                        </div>
                        <div class="content _change">
                            {{ str_replace(['<p>', '</p>'], '', $footer->footText) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
        let hidden = document.getElementById("hiddenIndex").value
        let adaptive_li = document.getElementsByClassName("adaptive_li")
        let liTag = document.getElementsByTagName('li')[hidden]
        let entered = true
        // let changableText = document.querySelectorAll('.change_')
        // for(let x = 0;x < changableText.length;x++){
        //     let content = changableText[x].innerText;
        //     changableText[x].innerHTML = content
        // }
        adaptive_li[hidden].classList.add('active')
        liTag.children[0].classList.add('active')
        document.querySelector(".burger").onclick = () =>{
            document.querySelector(".adaptive_menu").style.display = "block"
        }
        document.querySelector(".fa").onclick = () =>{
            document.querySelector(".adaptive_menu").style.display = "none"
        }
        document.querySelector(".modal-appear").onclick = (e)=>{
            e.preventDefault()
            if(entered) document.querySelector(".modal-body").classList.add("d-flex")
            else document.querySelector(".modal-body").classList.remove("d-flex")
            entered = entered?true:false
        }
        document.querySelector(".close-modal-body").onclick = ()=>{
            document.querySelector(".modal-body").classList.remove("d-flex")
        }
    </script>
    @stack('scripts')
</html>
