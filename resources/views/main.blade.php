@extends('welcome')
@section('style')
    <link rel="stylesheet" href="{{ asset('css/block.css') }}">
@endsection
@section('content')
    <div class="block">
        @foreach($carousel_images as $val)
            <div class="backgroundImage" style="background-image: url('{{ \TCG\Voyager\Facades\Voyager::image($val->image) }}')"></div>
        @endforeach
        <div class="container">
            <div class="row">
                <div class="block_inner">
                    <div class="block_inner_left">
                        <div class="img">
                            <a target="_blank" href="{{url('video/')}}">
                                <div class="img_spanChange">PLAY</div>
                            </a>
                            <div class="img_block">
                                <a href="{{url('video/')}}" target="_blank">
                                    <img src="{{asset('img/play.svg')}}" alt="">
                                </a>

                                <div class="loader">
                                    <div class="navigator"></div>
                                </div>
                                <div class="pause"><div></div><div></div></div>
                            </div>
                        </div>
                        <div class="content">
                            <span class="change_">{!! $main_widgets->header_content !!}</span>
                        </div>
                        <div class="button">
                            <a href="{{ route('rent', app()->getLocale()) }}" style="width: 100%">
                                <button>
                                    {{__('welcome.button.loan')}}
                                </button>
                            </a>
                        </div>
                    </div>
{{--                    <div class="block_inner_right ">--}}
{{--                        <div class="content">--}}
{{--                            <span class="left_counter">01</span>--}}
{{--                            <div class="swiper">--}}
{{--                                <div class="swiper_item active"></div>--}}
{{--                                <div class="swiper_item"></div>--}}
{{--                                <div class="swiper_item"></div>--}}
{{--                            </div>--}}
{{--                            <span class="right_counter">03</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="block_inner_logos">
{{--                        <a href="https://telegram.org/">--}}
{{--                        <img src="{{\TCG\Voyager\Facades\Voyager::image($social_logos[1]->image)}}" alt="">--}}
{{--                        </a>--}}
                        <a href="https://www.instagram.com/sunkar_info/?igshid=1m0p58zkrbmhf" target="_blank">
                        <img src="{{\TCG\Voyager\Facades\Voyager::image($social_logos[0]->image)}}" alt="">
                        </a>
{{--                        <a href="https://www.youtube.com/" target="_blank">--}}
{{--                        <img src="{{\TCG\Voyager\Facades\Voyager::image($social_logos[2]->image)}}" alt="">--}}
{{--                        </a>--}}
{{--                        <a href="https://www.facebook.com/" target="_blank">--}}
{{--                        <img src="{{\TCG\Voyager\Facades\Voyager::image($social_logos[3]->image)}}" alt="">--}}
{{--                        </a>--}}


{{--                        @foreach($social_logos as $val)--}}
{{--                            <img src="{{url('storage/'.$val->image )}}" alt="">--}}
{{--                        @endforeach--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="lines">
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
    </div>
    <div class="block_images">
        @foreach($posters as $poster)
            <div class="block_images_main" style="background-image: url('{{\TCG\Voyager\Facades\Voyager::image($poster->image)}}')">
                <div class="inner_content">

                    <div class="content">
                        <div class="content_block">
                            <div class="conent_block_in change_">
                                {!! $poster->distance !!}
                            </div>
                        </div>

{{--                        <div class="content_block">--}}
{{--                            <div class="conent_block_in change_">--}}
{{--                                <img src="{{\TCG\Voyager\Facades\Voyager::image($poster->image)}}" alt="">--}}
{{--                            </div>--}}
{{--                        </div>--}}

                        <div class="content_block">
                            <div class="conent_block_in change_">
                                {!! $poster->places !!}
                            </div>
                        </div>

                        <div class="content_block">
                            <div class="conent_block_in change_">
                                {!! $poster->width !!}
                            </div>
                        </div>

                    </div>
                </div>
                <div class="text change_">{!! $poster->title !!}</div>
            </div>
        @endforeach
    </div>
    <div class="block_perspectives">
        <div class="container">
            <div class="row">
                <div class="block_perspectives_left">
                    <div class="content">
                        <h3 class="change_">
                            {!! $perspectives->title !!}
                        </h3>
                        <div class="list">
                            @foreach($perspective_items as $item)
                                <div class="list_content">
{{--                                    <img src="{{ url('storage/').'/'.$item->image }}" alt="">--}}
                                    <img src="{{\TCG\Voyager\Facades\Voyager::image($item->image)}}" alt="">
                                    <div >
                                        <h3 class="change_">{!! $item->title !!}</h3>
                                        <span class="change_">{!! $item->description !!}</span>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>

                </div>
                <div class="block_perspectives_right">
                    <div class="image">
                        <img src="{{ url('storage/').'/'.$perspectives->image }}" alt="" height="100%">
                    </div>
                    <div class="content">
                        <h3 class="change_">{!! $perspectives->right_title !!}</h3>
                        <p class="change_"> {!! $perspectives->right_content !!}</p>
                        <div class="button">
                            <a href="{{ route('rent', app()->getLocale()) }}" style="width: 100%">
                                <button>
                                    {{__('welcome.button')}}
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block_partners">
        <div class="container">
            <div class="row">
                <h3 class="change_">{!! $main_widgets->logos_header !!}</h3>
                <div>
                    @foreach($logos as $logo)
                        <img src="{{\TCG\Voyager\Facades\Voyager::image($logo->image)}}" alt="">
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            let pos = 0
            let isDecrement = false
            let imgs = document.getElementsByClassName('backgroundImage')
            let swipers = document.getElementsByClassName('swiper_item')
            let videoTag = document.querySelector('.player-video')
            let pauseVideoTag = document.querySelector('.pause')
            let startVideoTag = document.querySelector('.img img')
            let texts = document.querySelectorAll('.block_images_main .text')
            let posterTexts = document.querySelectorAll(".conent_block_in")
            let spanChangeTag = document.querySelector('.img_spanChange')
            let navigatorTag = document.querySelector('.navigator')
            let prev = 0
            let images = document.querySelectorAll(".block_images_main")

            let check = (isDecrement) => {
                if(isDecrement) imgs[pos+1].style.top = "100vh"
                else imgs[pos].style.top = "0"
                document.querySelector('.left_counter').innerHTML = "0"+(pos+1)
            }
            let updateProgressBar = () =>{
                var percentage = Math.floor((100 / videoTag.duration) * videoTag.currentTime);
                navigatorTag.style.left = `${percentage}%`
            }
            let changeDots = (isDecrement) => {
                if(!isDecrement){
                    swipers[pos-1].style.backgroundColor = "rgba(255, 255, 255, 0.15)"
                    swipers[pos].style.backgroundColor = "white"
                }
                else{
                    swipers[pos+1].style.backgroundColor = "rgba(255, 255, 255, 0.15)"
                    swipers[pos].style.backgroundColor = "white"
                }
            }
            let thread = setInterval(() => {
                if(pos === 0) isDecrement = false
                else if(pos === 2) isDecrement = true
                if(isDecrement) pos--
                else pos++
                check(isDecrement)
                changeDots(isDecrement)
            }, 3000);

            function callFuncForMedia(){
                for(let x = 0;x < images.length;x++){
                    images[x].onclick = () => {
                        if(prev == x) return;
                        document.querySelector(`.block_images_main:nth-child(${prev+1})`).style.width = "30%"
                        document.querySelector(`.block_images_main:nth-child(${prev+1}) .inner_content`).style.transform = "scale(0,0)"
                        document.querySelector(`.block_images_main:nth-child(${x+1})`).style.width = "40%"
                        document.querySelector(`.block_images_main:nth-child(${x+1}) .inner_content`).style.transform = "scale(1,1)"
                        texts[x].style.width = "35%"
                        texts[prev].style.width = "80%"
                        prev = x
                    }
                }
            }

            function callFuncForMediaLow(){
                for(let x = 0;x < images.length;x++){
                    images[x].onclick = () => {
                        if(prev == x) return;
                        document.querySelector(`.block_images_main:nth-child(${prev+1}) .inner_content`).style.transform = "scale(0,0)"
                        document.querySelector(`.block_images_main:nth-child(${x+1}) .inner_content`).style.transform = "scale(1,1)"
                        texts[x].style.width = "35%"
                        texts[prev].style.width = "80%"
                        prev = x
                    }
                }
            }

            function myFunction(x) {
                if (x.matches) callFuncForMediaLow()
                else callFuncForMedia()
            }

            var x = window.matchMedia("(max-width: 698px)")
            myFunction(x)
            x.addListener(myFunction)

            // for(let x = 0;x < posterTexts.length;x++){
            //     let spanAppend = '<span class="active">'
            //     for(let y = 0;y < posterTexts[x].innerText.length;y++){
            //         if(isNaN(posterTexts[x].innerText[y]))  spanAppend = spanAppend + '</span>'

            //         spanAppend += posterTexts[x].innerText[y]
            //     }
            //     posterTexts[x].innerHTML = spanAppend
            // }
        </script>
    @endpush
@endsection
