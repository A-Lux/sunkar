@extends('welcome')
@section('style')
    <link rel="stylesheet" href="{{ asset('css/park.css') }}">
@endsection

@section('content')
<div class="news">
{{--    <div class="news_bg" style="background-image: url('{{ asset('storage/'.\App\Models\Banners::all()->first()["park-page"]) }}');"></div>--}}
    <div class="news_bg" style="background-image: url({{ \TCG\Voyager\Facades\Voyager::image($publications->post_image)}}"></div>
    <div class="news_content">
      <div class="container">
        <div class="row">
          <div class="col-md-12" style="padding: 0;">
            <div class="global_title">
              <h3>Sunkar Park</h3>
            </div>
            <div class="park_content">
              <p class="_change">
                {{ str_replace(['<p>', '</p>'], '', $content->content1) }}
              </p>
              <div class="park_content_img">
{{--                <img src="{{ url('storage').'/'.$content->image_first }}" alt="">--}}
{{--                <img src="{{ url('storage').'/'.$content->image_second }}" alt="">--}}
{{--                <img src="{{ url('storage').'/'.$content->image_third }}" alt="">--}}
                  <img src="{{\TCG\Voyager\Facades\Voyager::image($content->image_first)}}" alt="">
                  <img src="{{\TCG\Voyager\Facades\Voyager::image($content->image_second)}}" alt="">
                  <img src="{{\TCG\Voyager\Facades\Voyager::image($content->image_third)}}" alt="">
              </div>
              <p class="_change">
                {{ str_replace(['<p>', '</p>'], '', $content->content2) }}
              </p>
              <div class="park_content_single_img">
                  <img src="{{\TCG\Voyager\Facades\Voyager::image($content->image_fourth)}}" alt="">
              </div>
              <p class="_change">
                {{ str_replace(['<p>', '</p>'], '', $content->content3) }}
              </p>
              <div class="park_content_double_img">
                  <img src="{{\TCG\Voyager\Facades\Voyager::image($content->image_fifth)}}" alt="">
                  <img src="{{\TCG\Voyager\Facades\Voyager::image($content->image_sixth)}}" alt="">
              </div>
              <p class="_change">
                {{ str_replace(['<p>', '</p>'], '', $content->content4) }}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
