@extends('welcome')
@section('style')
    <link rel="stylesheet" href="{{ asset('css/news.css') }}">
@endsection
@section('content')
<div class="news">
    <div class="news_bg" style="background-image: url({{ \TCG\Voyager\Facades\Voyager::image($images->image_third)}}"></div>
    <div class="news_content">
      <div class="container">
        <div class="row">
          <div class="col-md-12" style="padding: 0;">
            <div class="global_title">
              <h3>{{__('welcome.news')}}</h3>
            </div>
{{--              <a href="{{ route('rent', app()->getLocale()) }}" style="width: 100%"--}}
            @foreach($men as $val)
{{--              <a href="{{ url('kz/news/detail'.'/'.$val->id) }}" class="news_content_items">--}}
{{--                  <a href="{{ url('/kz/news/detail'.'/'.$val->id) }}" class="news_content_items">--}}
{{--                  <a href="{{route('news_detail',$val->id, app()->getLocale())}}">--}}
{{--                  <a href="{{ route('news_detail', app()->getLocale())}} ">--}}
{{--                  <a href=" {{ route('news_detail',['id'=>$val->id])}}">--}}
{{--                      <a href="{{route('news_detail'. '/' . $val->id, app()->getLocale())}}"></a>--}}
                  <a href="{{route('news_detail',['lang'=>app()->getLocale(), 'id'=>$val->id])}}">
{{--                  <a href="{{route(('news') . '/' . $val->id, app()->getLocale())}}"></a>--}}
{{--                  <a href="{{ route('news_detail',['id'=>$val->id], app()->getLocale())}} "></a>--}}
{{--                  <a href="{{ route('news_detail',['id'=>$val->id, 'lang' => app()->getLocale()])}} "></a>--}}
{{--                  <a href="{{route('news_detail', ['id'=>$val->id], app()->getLocale())}}"></a>--}}
{{--                  <a href="{{  route('news_detail', '/' . ['id'=>$val->id], app()->getLocale()) }}"></a>--}}

                <div class="news_content_items__img">
                  <img src="{{ asset('storage/'.$val->post_image) }}" alt="">
                </div>
                <div class="news_content_items__descr">
                  <h3 class="news_content_items__descr__title _change">
                    {{ $val->post_title }}
                  </h3>
                  <p class="news_content_items__descr__content _change">
                    {{ $val->post_text }}
                  </p>
                  <span class="news_content_items__descr__date _change">
                    {{ Carbon\Carbon::parse($val->created_at)->day }}-
                    {{ Carbon\Carbon::parse($val->created_at)->month }}-
                    {{ Carbon\Carbon::parse($val->created_at)->year }}
                  </span>
                </div>
              </a>
            @endforeach


            <!-- <a href="javascript:void(0);" class="news_content_items">
              <div class="news_content_items__img">
                <img src="{{ asset('img/img/news_item_1.png') }}" alt="">
              </div>
              <div class="news_content_items__descr">
                <h3 class="news_content_items__descr__title">
                  People Like Meghan McCain
                  Are Why Americans Keep Dying

                </h3>
                <p class="news_content_items__descr__content">
                  Международный комплекс лыжных трамплинов построен в преддверии  седьмых Зимних Азиатских игр в 2010 году где было проведено 140 соревнов...
                </p>
                <span class="news_content_items__descr__date">
                  Sep 8 2020
                </span>
              </div>
            </a> -->
          </div>
        </div>
      </div>
    </div>
    <div class="news_pagination">
{{--        @dd($men)--}}
{{--      {{ $men->links('vendor.pagination.custom') }}--}}

      <!-- <a href="javascript:void(0);">
        <img src="{{ asset('img/img/arrow_prev.svg') }}" alt="">
        <img class="active_arrow" src="{{ asset('img/img/arrow_prev_active.svg') }}" alt="">
      </a>
      <a href="javascript:void(0);">1</a>
      <a href="javascript:void(0);">2</a>
      <a href="javascript:void(0);">3</a>
      <a href="javascript:void(0);">...</a>
      <a href="javascript:void(0);">50</a>
      <a href="javascript:void(0);">
        <img src="./assets/img/arrow_next.svg" alt="">
        <img class="active_arrow" src="{{ asset('img/img/arrow_next_active.svg') }}" alt="">
      </a> -->
    </div>
  </div>
@endsection
