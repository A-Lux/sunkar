
@if ($paginator->hasPages())
       
        @if ($paginator->onFirstPage())
            <img src="{{ asset('img/img/arrow_prev_active.svg') }}" alt="">
        @else
            <a href="{{ $paginator->previousPageUrl() }}" rel="prev"><img class="active_arrow" src="{{ asset('img/img/arrow_prev.svg') }}" alt=""></a>
        @endif

        @foreach ($elements as $element)
           
            @if (is_string($element))
                <a href=""><span>{{ $element }}</span></a>
            @endif
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <a href="" style="background-color: #26242B; color: white"><span>{{ $page }}</span></a>
                    @else
                        <a href="{{ $url }}">{{ $page }}</a>
                    @endif
                @endforeach
            @endif
        @endforeach
        
        @if ($paginator->hasMorePages())
        <a href="{{ $paginator->nextPageUrl() }}" rel="next">        <img src="{{ asset('img/img/arrow_next.svg') }}" alt=""></a>

        @else
        <img class="active_arrow" src="{{ asset('img/img/arrow_next_active.svg') }}" alt="">

        @endif
@endif 