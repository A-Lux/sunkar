<?php

return [
    'main' => 'Главная страница',
    'project' =>'О проекте',
    'park' => 'Парк',
    'services' => 'Услуги',
    'news' => 'Новости',
    'contacts' => 'Контакты',
    'button.loan' => 'Арендовать Трамплин',
    'button' => 'Арендовать',
    'text-us' => 'Напишите Нам',
    'about-us' => 'Про нас',
    'call' => 'Позвонить' ,
    'similar' => 'Похожие новости'
];

